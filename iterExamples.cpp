#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
void printv(const std::vector<int>& v){
  for(auto n: v)
    std::cout<<n<<" ";
  std::cout<<std::endl;
}
int main ()
{
  std::vector<int> myvector (5);  // 5 default-constructed ints

  int i=0;
  //reverse_iterator
  std::vector<int>::reverse_iterator rit = myvector.rbegin();
  for (; rit!= myvector.rend(); ++rit)
    *rit = i++;
  printv(myvector);
  
  //insert using iterators
  std::vector<int> nv = {5,6,7};
  myvector.insert(myvector.begin(), 10);
  myvector.insert(myvector.begin(), 2, 11);
  myvector.insert(myvector.end(), nv.begin(), nv.end());
  printv(myvector);
  
  //ostream iterator
  std::ostream_iterator<int> out_it (std::cout,", ");
  std::stringstream ss;
  std::ostream_iterator<int> s_iter(ss, ", ");

  for(auto iter = myvector.begin(); iter!=myvector.end(); iter++){
    //write to console
    *out_it = *iter;
    //write to stringstream
    *s_iter = *iter;
    out_it++;
    s_iter++;
  }
  std::cout<<std::endl;
  std::cout<<ss.str();

  return 0;
}
