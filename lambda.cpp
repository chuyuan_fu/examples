#include <functional>
#include <algorithm>
#include <iostream>  
#include <vector>

/*  Note:
    Each lambda here may have different parameter list and return type
    because of the specific requirement of each functions they are called with.
    Additional reference
    https://msdn.microsoft.com/en-us/library/dd293603.aspx
    https://msdn.microsoft.com/en-us/library/dd293599.aspx
*/
using namespace std;

bool greater20(int x){
  return x>20;
}
int main()  
{  
  
  vector<int> my_vector = {10,20,30,30,40};
  // ref: http://www.cplusplus.com/reference/algorithm/count/
  cout<<count(my_vector.begin(), my_vector.end(), 20)<<endl;
  cout<<count(my_vector.begin(), my_vector.end(), 30)<<endl;
    
  //ref: http://www.cplusplus.com/reference/algorithm/count_if/
  //provide a function
  cout<<count_if(my_vector.begin(), my_vector.end(), greater20)<<endl;
  //saving a lambda that can be used later
  auto g20 = [](int x) { return x >20; };
  cout<<count_if(my_vector.begin(), my_vector.end(), g20)<<endl;
  //providing a lambda. compare this lambda with the function greater20
  cout<<count_if(my_vector.begin(), my_vector.end(),  [](int x) 
		 { return x >20;}   )<<endl;
    
  //sort according to descending order
  //ref: http://www.cplusplus.com/reference/algorithm/sort/
  sort(my_vector.begin(),my_vector.end(),[](int x, int y){return x>y;});
    
  //two different cout formats using different lamdbas
  //for_each is a very general function
  //input is one element
  //you can do in the function body whatever you can do in any function
  for_each(my_vector.begin(),my_vector.end(),[](int x){cout<<x<<" ";});
  auto opl = [](int x){cout<<x<<", ";};
  for_each(my_vector.begin(), my_vector.end(), opl); cout<<endl;
    
  //generate assigns the value returned by successive calls to 
  // gen (provided as lambda here) to the elements in the range [first,last).
  int n = 0;
  generate(my_vector.begin(), my_vector.end(), [&n](){n++; return n;});
  for_each(my_vector.begin(), my_vector.end(), opl); cout<<endl;
  
}  

