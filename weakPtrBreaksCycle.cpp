#include <iostream>
#include <memory>
 
struct A
{
  A() { std::cout << "A \n"; }
  ~A() { std::cout << "~A \n"; }
  //replace the weak_ptr memory will be leaked
  std::weak_ptr<A> ptr;
};
 

int main()
{
  {
    std::shared_ptr<A> x = std::make_shared<A>();
    std::shared_ptr<A> y = std::make_shared<A>();
    x->ptr = y;
    y->ptr = x;
  }
}


